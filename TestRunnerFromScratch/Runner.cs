﻿namespace TestRunnerFromScratch
{
    public static class Runner
    {
        public static void Describe(string description, Action block)
        {
            Console.WriteLine(description);
            block();
        }

        public static void It(string description, Action block)
        {
            Console.Write($"  - {description}");
            try
            {
                block();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" (ok)");
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" (fail)");
                Console.ResetColor();

                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.ToString());   // print backtrace
            }
        }

        public static void ShouldBe(this int actual, int expected)
        {
            if (actual != expected)
            {
                throw new AssertionError($"Expected {expected} but got {actual}");
            }
        }
    }

    internal class AssertionError : Exception
    {
        public AssertionError(string? message) : base(message)
        {
        }
    }
}
