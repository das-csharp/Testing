﻿using static TestRunnerFromScratch.Runner;

namespace TestRunnerFromScratch
{
    public class Program
    {
        static void Main()
        {
            Describe("some examples", () =>
            {
                It("can pass", () =>
                {
                    (1 + 1).ShouldBe(2);
                });

                It("can fail", () =>
                {
                    (1 + 1).ShouldBe(3);
                });

                It("can pass after failing", () =>
                {

                });
            });
        }
    }
}
